Photo Upload App
================
----------------

Photo Upload is the Android app for uploading photos taken by user to AWS S3 bucket.

This project is a demo project for the interview. The app supports devices
running Android 4.4+.

Features
--------
With the app, you can:

- Take a photo
- View taken photo
- Upload the photo